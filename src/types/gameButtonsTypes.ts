export type gameButtonProps = {
	x: number;
	y: number;
	hidden: boolean;
	points: number;
	bomb: boolean;
	cellClick(x: number, y: number): void;
};
