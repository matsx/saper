export type scoreStatusTypes = {
	score: number;
	top?: number;
	color?: string;
};
