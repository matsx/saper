import {BoardCell} from '../board/Board';

export type AppProps = {
	boardElements: BoardCell[];
	cellClick(x: number, y: number): void;
};
