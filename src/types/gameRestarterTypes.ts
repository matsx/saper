export type gameRestarterTypes = {
	restartClick(): void;
	top?: number;
	status: string;
};
