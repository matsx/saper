import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import {useSelector, useDispatch} from 'react-redux';

type props = {
	status: string;
	firstClick: boolean;
};

const TimerContainer = styled.div`
	position: absolute;
	z-index: 5;
	width: 100px;
	@media (max-width: 768px) {
		left: calc(50% - 50px);
		top: 10px;
	}
`;

const StyledSpan = styled.span`
	position: absolute;
	color: white;
	top: 30px;
	font-size: 28px;
	left: 11px;
`;

const Timer = (props: props) => {
	const [timer, setTimerValue] = useState<number>(0);
	const timerRecords = useSelector((state: any) => state.bestTime);
	const formValues = useSelector((state: any) => state.gameValues);
	const dispatch = useDispatch();

	useEffect(() => {
		if (props.firstClick) {
			setTimerValue(0);
		}
	}, [props.firstClick]);

	useEffect(() => {
		let timeout: null | number = null;
		if (props.firstClick === false && props.status === 'game') {
			timeout = setTimeout(() => {
				setTimerValue(timer + 1);
			}, 1000);
		}
		if (props.status === 'win' || props.status === 'defat') {
			dispatch({
				type: 'SET_CURRENT_SCORE',
				value: {
					score: timer,
					level: formValues.diffLevel,
				},
			});

			if (
				(timer < timerRecords[formValues.diffLevel] ||
					!timerRecords[formValues.diffLevel]) &&
				props.status === 'win'
			) {
				dispatch({
					type: 'SET_BEST_TIME',
					value: {
						level: formValues.diffLevel,
						score: timer,
					},
				});
			}
		}
		return () => {
			if (timeout !== null) {
				clearTimeout(timeout);
			}
		};
	}, [timer, props.firstClick, props.status]);
	return (
		<TimerContainer className='base-timer'>
			<svg
				className='base-timer__svg'
				viewBox='0 0 100 100'
				xmlns='http://www.w3.org/2000/svg'
			>
				<g className='base-timer__circle'>
					<circle
						className='base-timer__path-elapsed'
						cx='50'
						cy='50'
						r='45'
					/>
					<path
						id='base-timer-path-remaining'
						className='base-timer__path-remaining'
						d='
                        M 50, 50
                        m -45, 0
                        a 45,45 0 1,0 90,0
                        a 45,45 0 1,0 -90,0'
						strokeDasharray={`${
							(timer - Math.floor(timer / 60) * 60) * 4.7
						} 314`}
					></path>
				</g>
			</svg>
			<StyledSpan>
				{`${
					Math.floor(timer / 60) < 10
						? '0' + String(Math.floor(timer / 60))
						: Math.floor(timer / 60)
				}:
                ${
					timer - Math.floor(timer / 60) * 60 < 10
						? '0' + String(timer - Math.floor(timer / 60) * 60)
						: timer - Math.floor(timer / 60) * 60
				}`}
			</StyledSpan>
		</TimerContainer>
	);
};

export default Timer;
