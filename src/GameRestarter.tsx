import React from 'react';
import {gameRestarterTypes} from './types/gameRestarterTypes';
import styled from 'styled-components';
import {useSelector} from 'react-redux';
import Flip from 'react-reveal/Flip';

const WinMessage = styled.h1`
	text-align: center;
	color: black;
	display: block;
	margin-bottom: 15%;
`;

const StyledParagraph = styled.p`
	display: block;
	text-align: center;
`;

const StyledContainer = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0%;
	left: 0%;
	z-index: 2;
	color: black;
	background-color: rgba(255, 255, 255, 0.15);
	backdrop-filter: blur(5px);
`;

const RestartButton = styled.button`
	width: 200px;
	height: 60px;
	background-color: #1a1423;
	color: white;
	cursor: pointer;
	border-radius: 5px;
	border: 0px;
	text-align: center;
	margin-left: calc(50% - 100px);
	position: relative;
	margin-top: 25%;
	font-size: 19px;
`;

const MessagesContainer = styled.div`
	width: 350px;
	height: 355px;
	position: absolute;
	left: calc(50% - 175px);
	top: calc(50% - 175px);
	background-color: rgba(255, 255, 255, 0.75);
	border-radius: 5px;
`;

const GameRestarter = (props: gameRestarterTypes) => {
	const lastGameTime = useSelector((state: any) => state.score.score);
	const record = useSelector(
		(state: any) => state.bestTime[state.score.level]
	);

	return (
		<StyledContainer>
			<MessagesContainer>
				{props.status === 'win' ? (
					<WinMessage>You won!</WinMessage>
				) : (
					<WinMessage>You lose, try again!</WinMessage>
				)}
				<StyledParagraph>
					Best time: <b>{record ? record : '-- : --'}</b>s.
				</StyledParagraph>
				{lastGameTime ? (
					<StyledParagraph
						style={
							props.status === 'defat'
								? {color: 'red'}
								: {color: 'black'}
						}
					>
						Current time: <b>{lastGameTime}</b>s.
					</StyledParagraph>
				) : null}
				<Flip bottom>
					<RestartButton onClick={props.restartClick}>
						Restart
					</RestartButton>
				</Flip>
			</MessagesContainer>
		</StyledContainer>
	);
};

export default GameRestarter;
