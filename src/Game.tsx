import React, {useState, useEffect, useCallback} from 'react';
import './App.css';
import {createBombsPositionsArr as bombsArray} from './board/BombsPosition';
import {createBoard, BoardCell} from './board/Board';
import GameBoard from './Gameboard';
import GameRestarter from './GameRestarter';
import {unhideNeighboring} from './board/UnhideNeighboring';
import {useSelector} from 'react-redux';
import Timer from './Timer';

const computeBoard = (level: string): BoardCell[] => {
	let computedBoard;
	switch (level) {
		case 'easy':
			computedBoard = createBoard(bombsArray(10));
			break;
		case 'medium':
			computedBoard = createBoard(bombsArray(20));
			break;
		case 'hard':
			computedBoard = createBoard(bombsArray(30));
			break;
	}
	return computedBoard ? computedBoard : createBoard(bombsArray());
};

const Game = () => {
	const formValues = useSelector((state: any) => state.gameValues);
	const [board, setBoardState] = useState<BoardCell[]>(
		computeBoard(formValues.diffLevel)
	);
	const [gameStatus, setGameStatus] = useState('game');
	const [score, setScore] = useState(0);
	const [firstClick, setFirstClick] = useState(true);

	useEffect(() => {
		if (gameStatus === 'defat') {
			const updatedBoard: BoardCell[] = board.map((el) => {
				if (el.bomb) {
					el.hidden = false;
				}
				return el;
			});
			setBoardState(updatedBoard);
		}
	}, [gameStatus]);

	const cellClick = useCallback(
		(x: number, y: number): void => {
			let updatedIndex: number | null = null;
			const updatedBoard: BoardCell[] = board.map((el) => {
				if (el.position[0] === x && el.position[1] === y) {
					if (el.hidden) {
						el.hidden = false;
						updatedIndex = board.indexOf(el);
						if (el.bomb && !firstClick) {
							setGameStatus('defat');
						} else if (el.bomb && firstClick) {
							el.bomb = false;
							el.points = el.points - 1;
							el.neighbours.forEach((neighbourIndex) => {
								board[neighbourIndex].points =
									board[neighbourIndex].points - 1;
							});
							setScore(el.points - 1);
						} else {
							setScore(score + el.points);
						}
					}
				}
				return el;
			});
			if (updatedIndex !== null && !board[updatedIndex].points) {
				unhideNeighboring(updatedBoard, updatedIndex);
			}
			setBoardState(updatedBoard);
			let hiddenFields = 0;
			updatedBoard.forEach((el) => {
				if (el.hidden && !el.bomb) {
					hiddenFields++;
				}
			});
			if (hiddenFields === 0) {
				setGameStatus('win');
			}
			setFirstClick(false);
		},
		[score, gameStatus, firstClick]
	);

	const handleRestartClick = () => {
		const newBoard = computeBoard(formValues.diffLevel);
		setBoardState(newBoard);
		setGameStatus('game');
		setScore(0);
		setFirstClick(true);
	};
	return (
		<>
			{gameStatus === 'defat' || gameStatus === 'win' ? (
				<GameRestarter
					restartClick={handleRestartClick}
					status={gameStatus}
				/>
			) : null}
			<Timer status={gameStatus} firstClick={firstClick} />
			<GameBoard boardElements={board} cellClick={cellClick} />
		</>
	);
};

export default Game;
