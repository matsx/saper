import React from 'react';
import {scoreStatusTypes} from './types/scoreStatusTypes';
import styled from 'styled-components';

const StyledDiv = styled.div`
	position: absolute;
	top: 5%;
	left: 40%;
	color: white;
	width: 20%;
	text-align: center;
	font-size: large;
`;

const ScoreStatus = (props: scoreStatusTypes) => {
	return (
		<StyledDiv
			style={{
				top: props.top ? `${props.top}%` : undefined,
				color: props.color ? `${props.color}` : 'white',
			}}
		>
			<span>Score: </span>
			<span>{props.score}</span>
		</StyledDiv>
	);
};

export default ScoreStatus;
