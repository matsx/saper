import React from 'react';
import {useForm} from 'react-hook-form';
import styled from 'styled-components';
import {useDispatch} from 'react-redux';
import Pulse from 'react-reveal/Pulse';

const FormContainer = styled.div`
	position: absolute;
	width: 340px;
	height: 300px;
	top: calc(50% - 150px);
	left: calc(50% - 170px);
	border-radius: 5px;
	background-color: #fff;
	z-index: 2;
`;

const StyledLabel = styled.label`
	font-size: 17px;
	font-smooth: always;
	margin-left: 15px;
	line-height: 3;
	display: block;
`;

const NameInput = styled.input`
	display: block;
	height: 30px;
	border: 1px solid grey;
	margin-left: 15px;
	width: 85%;
`;

const FormButton = styled.input`
	display: inline-block;
	width: 80px;
	margin-left: 20px;
	height: 30px;
	border: 1px solid black;
	background-color: transparent;
	border-radius: 5px;
	&:hover {
		background-color: #1a1423;
		color: white;
		cursor: pointer;
	}
`;

const SubmitButton = styled.input`
	bottom: 40px;
	position: relative;
	left: 70px;
	width: 200px;
	height: 34px;
	border-radius: 5px;
	border: 1px solid black;
	background: transparent;
	cursor: pointer;
	margin-top: 27%;
`;

const CheckboxLabel = styled.label`
	position: absolute;
	margin-top: 35px;
	margin-left: -60px;
`;

const MediumLabel = styled.label`
	position: absolute;
	margin-top: 35px;
	margin-left: -73px;
`;

const StyledSpan = styled.span`
	color: red;
	padding-left: 15px;
`;

const GameSettings = () => {
	const dispatch = useDispatch();
	const {register, handleSubmit, watch, errors} = useForm();
	const nameInput = watch('name');

	const onSubmit = (values: any) => {
		dispatch({type: 'GAME_VALUES', value: values});
	};

	return (
		<FormContainer>
			<form onSubmit={handleSubmit(onSubmit)} autoComplete='off'>
				<StyledLabel htmlFor='name'>Name:</StyledLabel>
				<NameInput
					type='text'
					name='name'
					id=''
					ref={(e) => register(e, {required: true})}
				/>
				{errors.name && (
					<StyledSpan>Please insert your name.</StyledSpan>
				)}
				<StyledLabel htmlFor='diffLevel'>Difficulty level:</StyledLabel>
				<FormButton
					id='easy'
					type='radio'
					name='diffLevel'
					value='easy'
					ref={(e) => register(e)}
					defaultChecked={true}
				/>
				<CheckboxLabel htmlFor='easy'>easy</CheckboxLabel>
				<FormButton
					type='radio'
					name='diffLevel'
					value='medium'
					ref={(e) => register(e)}
				/>
				<MediumLabel htmlFor='medium'>medium</MediumLabel>
				<FormButton
					type='radio'
					name='diffLevel'
					value='hard'
					ref={(e) => register(e)}
				/>
				<CheckboxLabel htmlFor='hard'>hard</CheckboxLabel>
				<Pulse spy={nameInput}>
					<SubmitButton
						type='submit'
						value='start'
						style={
							nameInput
								? {color: 'white', backgroundColor: '#1A1423'}
								: {}
						}
					/>
				</Pulse>
			</form>
		</FormContainer>
	);
};

export default GameSettings;
