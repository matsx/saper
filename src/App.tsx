import React from 'react';
import './App.css';
import styled from 'styled-components';
import GameSettings from './GameSettings';
import {useSelector} from 'react-redux';
import Game from './Game';

const StyledBackground = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: #1a1423;
`;

function App() {
	const formValues = useSelector((state: any) => state.gameValues);
	return (
		<>
			<StyledBackground></StyledBackground>
			{!formValues ? <GameSettings /> : <Game />}
		</>
	);
}

export default App;
