type Bomb = [number, number];
export type BombArray = Bomb[];

export const createBombsPositionsArr = (bombsValue = 10): BombArray => {
	let bombsPositions: BombArray = [];
	for (let i = 0; i < bombsValue; i++) {
		const x = Math.floor(Math.random() * 10) + 1;
		const y = Math.floor(Math.random() * 10) + 1;
		const bombPosition: Bomb = [x, y];
		bombsPositions.push(bombPosition);
	}
	return bombsPositions;
};
