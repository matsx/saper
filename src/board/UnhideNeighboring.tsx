import {BoardCell} from "./Board"

export const unhideNeighboring = (board:BoardCell[], index:number, recursiveCall?:true):BoardCell[] => {
    board[index].neighbours.forEach((el) => {
        if(board[el].hidden && !board[el].bomb) {
                board[el].hidden = false;
                if(!board[el].points) {
                    unhideNeighboring(board, el, true)
            }
        }
    })
    return board;
}