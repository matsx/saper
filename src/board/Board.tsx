import {BombArray} from './BombsPosition';

export interface BoardCell {
	position: number[];
	bomb: boolean;
	points: number;
	hidden: boolean;
	neighbours: number[];
}

const checkIfBomb = (
	boardElement: number[],
	bombPositions: number[][]
): boolean => {
	let isBomb: boolean = false;
	bombPositions.forEach((el: number[]) => {
		if (!isBomb) {
			isBomb = JSON.stringify(el) === JSON.stringify(boardElement);
		}
	});
	return isBomb;
};

const setNeighboursIndexesArray = (cell: number[]): number[] => {
	const neighbours = [];
	const index = (cell[1] - 1) * 10 + cell[0] - 1;
	for (let i = 0; i <= 2; i++) {
		if (index - 10 >= 0) {
			if (Number(index.toString()[1]) == 0) {
				if (i !== 0) {
					neighbours.push(index + i - 11);
				}
			} else if (Number(index.toString()[1]) == 9) {
				if (i !== 2) {
					neighbours.push(index + i - 11);
				}
			} else {
				neighbours.push(index + i - 11);
			}
		}
	}
	for (let i = 0; i < 1; i++) {
		if (Number(index.toString()[1]) == 0 || index === 0) {
			neighbours.push(index + 1);
		} else if (Number(index.toString()[1]) == 9 || index === 9) {
			neighbours.push(index - 1);
		} else {
			neighbours.push(index - 1);
			neighbours.push(index + 1);
		}
	}
	for (let i = 0; i <= 2; i++) {
		if (index + i + 9 < 100) {
			if (Number(index.toString()[1]) == 0 || index === 0) {
				if (i !== 0) {
					neighbours.push(index + i + 9);
				}
			} else if (Number(index.toString()[1]) == 9 || index === 9) {
				if (i !== 2) {
					neighbours.push(index + i + 9);
				}
			} else {
				neighbours.push(index + i + 9);
			}
		}
	}
	return neighbours;
};

export const createBoard = (bombs: BombArray): BoardCell[] => {
	const board = [];
	for (let i = 1; i <= 10; i++) {
		for (let ii = 1; ii <= 10; ii++) {
			board.push({
				position: [ii, i],
				bomb: checkIfBomb([ii, i], bombs),
				hidden: true,
				points: 0,
				neighbours: setNeighboursIndexesArray([ii, i]),
			});
		}
	}
	return assignPoints(board);
};

export const assignPoints = (bombArray: BoardCell[]): BoardCell[] => {
	bombArray.forEach((el: BoardCell): void => {
		if (el.bomb) {
			bombArray.forEach((element: BoardCell) => {
				if (
					(element.position[0] - el.position[0] === 1 ||
						element.position[0] - el.position[0] === -1 ||
						element.position[0] - el.position[0] === 0) &&
					(element.position[1] - el.position[1] === 1 ||
						element.position[1] - el.position[1] === -1 ||
						element.position[1] - el.position[1] === 0)
				) {
					element.points++;
				}
			});
		}
	});
	return bombArray;
};
