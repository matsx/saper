import React from 'react';
import {gameButtonProps} from './types/gameButtonsTypes';
import styled from 'styled-components';

const StyledCell = styled.div`
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-o-user-select: none;
	user-select: none;
	width: 48px;
	height: 48px;
	margin-top: 1px;
	margin-left: 1px;
	border-inline: 2px;
	background-color: white;
	position: absolute;
	text-align: center;
	line-height: 300%;
	@media (max-width: 768px) {
		width: 34px;
		height: 34px;
		line-height: 2.1;
	}
`;
const pointsToColor = new Map([
	['0', 'transparent'],
	['1', 'blue'],
	['2', 'green'],
	['3', 'red'],
	['4', 'red'],
	['5', 'red'],
]);

const GameButtons = (props: gameButtonProps) => {
	return (
		<StyledCell
			onClick={() => props.cellClick(props.x, props.y)}
			style={{
				left: `${props.x * 10 - 10}%`,
				top: `${props.y * 10 - 10}%`,
				backgroundColor: `${
					props.hidden ? 'darkgrey' : props.bomb ? 'red' : 'white'
				}`,
				color: pointsToColor.get(String(props.points)),
			}}
		>
			{!props.hidden && !props.bomb
				? props.points
				: !props.hidden && props.bomb
				? 'B'
				: ''}
		</StyledCell>
	);
};

export default GameButtons;
