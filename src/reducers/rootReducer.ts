interface rootReducerState {
	gameValues: null | {name: string; diffLevel: string};
	bestTime: {
		easy: null | number;
		medium: null | number;
		hard: null | number;
	};
	score: {
		score: number | null;
		level: string | null;
	};
}

const rootReducerState: rootReducerState = {
	gameValues: null,
	bestTime: {
		easy: null,
		medium: null,
		hard: null,
	},
	score: {
		score: null,
		level: null,
	},
};

export default function rootReducer(state = rootReducerState, action: any) {
	switch (action.type) {
		case 'GAME_VALUES':
			return {
				...state,
				gameValues: action.value,
			};
		case 'SET_BEST_TIME':
			return {
				...state,
				bestTime: {
					...state.bestTime,
					[action.value.level]: action.value.score,
				},
			};
		case 'SET_CURRENT_SCORE':
			return {
				...state,
				score: {
					score: action.value.score,
					level: action.value.level,
				},
			};
	}
	return state;
}
