import React from 'react';
import GameButtons from './GameButtons';
import styled from 'styled-components';
import {AppProps} from './types/appTypes';

const StyledGameBoard = styled.div`
	position: absolute;
	width: 500px;
	height: 500px;
	box-sizing: border-box;
	background-color: grey;
	position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
	@media (max-width: 768px) {
		width: 350px;
		height: 350px;
	}
`;

const GameBoard = (props: AppProps) => {
	const renderButtons = (): any => {
		const cells = props.boardElements.map((el) => {
			return (
				<GameButtons
					x={el.position[0]}
					y={el.position[1]}
					hidden={el.hidden}
					points={el.points}
					bomb={el.bomb}
					cellClick={props.cellClick}
				/>
			);
		});
		return cells;
	};
	return <StyledGameBoard>{renderButtons()}</StyledGameBoard>;
};

export default GameBoard;
